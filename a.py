import pygame
from sys import exit
from random import randint
import mysql.connector

def display_score():
    current_time = int(pygame.time.get_ticks() / 1000) - start_timer
    score_surface = test_font.render(f'Score:{current_time}', False, (0, 0, 0))
    score_rect = score_surface.get_rect(center=(320, 30))
    screen.blit(score_surface, score_rect)
    return current_time

def obstacle_movement(obstacle_list):
    if obstacle_list:
        for obstacle_rect in obstacle_list:
            obstacle_rect.x -= 5
            screen.blit(Enemy_surface, obstacle_rect)
        obstacle_list = [obstacle for obstacle in obstacle_list if obstacle.x > -100]
        return obstacle_list
    else:
        return []

def collisions(player, obstacles):
    if obstacles:
        for obstacle_rect in obstacles:
            if player.colliderect(obstacle_rect):
                return False
    return True

db = mysql.connector.connect(
    host="127.0.0.1",
    user="root",
    password="senha",
    database="game_highscores"
)
cursor = db.cursor()

pygame.init()
screen = pygame.display.set_mode((640, 288))
clock = pygame.time.Clock()
test_font = pygame.font.Font(None, 30)
game_active = False
start_timer = 0
score = 0
global_highscore = 0

sky_surface = pygame.image.load('Sky.png')
floor_surface = pygame.image.load('Floor.png')

intro_surface = test_font.render(f'Pressione ESPAÇO para começar', False, (0, 0, 0))
intro_rect = intro_surface.get_rect(center=(320, 144))

Enemy_surface = pygame.image.load('EnemyWal01.png')
enemy_rect = Enemy_surface.get_rect(midbottom=(600, 192))

obstacle_rect_list = []

player_surface = pygame.image.load('ChaWal01.png')
player_rect = player_surface.get_rect(midbottom=(20, 192))
player_grav = 0

obstacle_timer = pygame.USEREVENT + 1
pygame.time.set_timer(obstacle_timer, 1300)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
        if game_active:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE and player_rect.bottom >= 192:
                    player_grav = -15
        else:
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                game_active = True
                enemy_rect.left = 640
                start_timer = int(pygame.time.get_ticks() / 1000)

        if event.type == obstacle_timer:
            obstacle_rect_list.append(Enemy_surface.get_rect(midbottom=(randint(900, 1100), 192)))

    if game_active:
        screen.blit(sky_surface, (0, 0))
        screen.blit(floor_surface, (0, 192))
        score = display_score()

        player_grav += 1
        player_rect.y += player_grav
        if player_rect.bottom >= 192:
            player_rect.bottom = 192
        screen.blit(player_surface, player_rect)

        obstacle_rect_list = obstacle_movement(obstacle_rect_list)

        game_active = collisions(player_rect, obstacle_rect_list)

    else:
        screen.fill((94, 129, 162))
        morte_surface = test_font.render(f'iih mal tankou {score} segundos com a jiboia', False, (0, 0, 0))
        morte_rect = morte_surface.get_rect(center=(320, 144))
        obstacle_rect_list.clear()

        if score >= 1:
            query = "INSERT INTO highscores (score) VALUES (%s)"
            cursor.execute(query, (score,))
            db.commit()
            if score > global_highscore:
                global_highscore = score

        if global_highscore > 0:
            global_highscore_text = test_font.render(f'Global Highscore: {global_highscore}', False, (0, 0, 0))
            global_highscore_rect = global_highscore_text.get_rect(center=(320, 120))
            screen.blit(global_highscore_text, global_highscore_rect)

        if score >= 1:
            score_text = test_font.render(f'Your Score: {score}', False, (0, 0, 0))
            score_rect = score_text.get_rect(center=(320, 160))
            screen.blit(score_text, score_rect)

        if score == 0:
            screen.blit(intro_surface, intro_rect)

    pygame.display.update()
    clock.tick(60)

cursor.close()
db.close()

#cd C:\Users\Usuario\Desktop\Pygame
#python app.py

