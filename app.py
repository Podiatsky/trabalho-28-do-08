from flask import Flask, render_template
import mysql.connector

app = Flask(__name__)

db = mysql.connector.connect(
    host="127.0.0.1",
    user="root",
    password="senha",
    database="game_highscores"
)
cursor = db.cursor()

@app.route('/')
def index():
    cursor.execute("SELECT score, created_at FROM highscore ORDER BY score DESC, created_at ASC LIMIT 10")
    highscores = cursor.fetchall()
    return render_template('index.html', highscore=highscores)

if __name__ == '__main__':
    app.run(debug=True)
